//
//  SignalAnalyzerProtocol.swift
//  MAWITest
//
//  Created by admin on 07/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RxSwift
protocol SignalAnalyzerProtocol {
    func append(signalChunk: [Int16])
    var errorsInRow: PublishSubject<Int> {get}
}
