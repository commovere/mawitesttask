//
//  SignalAnalizer.swift
//  MAWITest
//
//  Created by admin on 03/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RxSwift
class SignalAnalizer: SignalAnalyzerProtocol {
    enum AnalyzeError: Error { case toMuchErrors }
    
    let errorsInRow = PublishSubject<Int>()
    private var errorCount = 0
    private var allData = [Int16]()
    
    func append(signalChunk: [Int16]){
        self.allData.append(contentsOf: signalChunk)
        guard self.allData.count >= 1000 else { return }
        
        let window = self.allData[0..<1000]
        self.allData = Array(self.allData.suffix(from: 1000))
        
        for i in 0..<4 {
            let side = i * 250
            let range = side..<(side + 250)
            let isValid = window[range].reduce(0, ^) >= 0
            self.errorCount = isValid ? 0 : self.errorCount + 1
            
            self.errorsInRow.onNext(self.errorCount)
        }
    }
}
