//
//  MeasurementView.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import Charts
import RxCocoa
class MeasurementView: UIView {
    private weak var chart: LineChartView!
    private weak var stopButton: UIButton!
    private weak var stateLabel: UILabel!
    private var viewModel: MeasurementViewModel!
    //MARK:- init
       init(_ viewModel: MeasurementViewModel) {
           self.viewModel = viewModel
           super.init(frame: .zero)
           self.setupComponents()
           self.setupViewModel()
       }
       
       required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    //MARK:- LyfeCycle (overrides)
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.viewModel == nil { return }
        
        self.stopButton.pin
            .bottom(pin.safeArea.bottom + 10)
            .left(pin.safeArea.left + 10)
        self.stateLabel.pin
        .width(220)
            .right(10)
            .vCenter(to: self.stopButton.edge.vCenter)
        self.chart.pin
            .top(pin.safeArea)
            .horizontally()
            .above(of: self.stopButton).marginBottom(10)
    }
    //MARK:- private
    private func setupComponents() {
        if self.viewModel == nil { return }
        self.backgroundColor = .lightGray
        
        self.chart = LineChartView().add(to: self)
        self.chart.backgroundColor = .darkGray
        self.chart.rightAxis.enabled = false
        let yAxis = self.chart.leftAxis
        yAxis.axisMinimum = -4000
        yAxis.axisMaximum = 4000
        
        self.stopButton = UI.btn("STOP").add(to: self)
        self.stateLabel = UI.lbl("State: ...").add(to: self)
        
        self.layoutIfNeeded()
    }
    
    private func setupViewModel() {
        self.viewModel.dataSource
            .bind{[weak self] data in
                DispatchQueue.main.async { self?.chart.data = data }
            }
            .disposed(by: self.viewModel.bag)
        self.viewModel.state
            .bind(to: self.stateLabel.rx.text)
            .disposed(by: self.viewModel.bag)
        self.stopButton.rx.tap
            .bind{[weak self] in self?.viewModel.stop(.byUser)}
            .disposed(by: self.viewModel.bag)
    }
}
