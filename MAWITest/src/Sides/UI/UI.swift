//
//  UI.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit
struct UI {
    static private let defaultUIFrame = CGRect(x: 0, y: 0, width: 100, height: 30)
    static func btn(_ title: String) -> UIButton {
        let btn = UIButton(frame: defaultUIFrame)
        btn.backgroundColor = .darkGray
        btn.layer.cornerRadius = 8
        btn.titleLabel?.font = .systemFont(ofSize: 20, weight: .semibold)
        btn.contentHorizontalAlignment = .center
        btn.setTitle(title, for: .normal)
        btn.setTitleColor(.lightGray, for: .normal)
        return btn
    }
    
    static func lbl(_ txt: String) -> UILabel {
        let lbl = UILabel(frame: defaultUIFrame)
        lbl.font = .systemFont(ofSize: 20, weight: .semibold)
        lbl.text = txt
        lbl.textColor = .black
        lbl.sizeToFit()
        return lbl
    }
}
