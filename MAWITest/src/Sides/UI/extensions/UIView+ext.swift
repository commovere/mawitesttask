//
//  UIView+ext.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit
extension UIView {
    @discardableResult func add(to parent: UIView) -> Self {
        parent.addSubview(self)
        return self
    }
}
