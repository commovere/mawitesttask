//
//  RootCoordinator.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit
import RxSwift

struct RootCoordinator: MeasurementListCoordinaror, MeasurementScreenCoordinator {
    private var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        self.startMeasurementListScreeen()
    }
    //MARK:-MeasurementListCoordinaror
    func showMeasurement(_ mode: ShowMeasurementMode) {
        self.startMeasurementScreeen(mode)
    }
    
    func requestNewMeasurement() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Start new measurement?", message: nil, preferredStyle: .alert)
            let fiveMinAction = UIAlertAction(title: "5 minutes", style: .default, handler: {_ in self.showMeasurement(.new5Minutes)})
            let infiniteAction = UIAlertAction(title: "Endless", style: .default, handler: {_ in self.showMeasurement(.newEndless)})
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(fiveMinAction)
            alert.addAction(infiniteAction)
            alert.addAction(cancelAction)
            
            self.navigationController.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:-MeasurementScreenCoordinator
    func stopMeasurement(_ reason: MeasurementViewModel.StopReason) {
        DispatchQueue.main.async {
            self.navigationController.popToRootViewController(animated: true)
            let msg = "Reason: \(reason.rawValue)"
            let alert = UIAlertController(title: "Measurement stoped", message: msg, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let vc = self.navigationController.topViewController
            vc?.present(alert, animated: true, completion: nil)
        }
    }
    //MARK:-private
    private func startMeasurementListScreeen() {
        let vc = MeasurementListVC()
        let vm = MeasurementListViewModel(self)
        vc.vm = vm
        self.navigationController.pushViewController(vc, animated: false)
    }
    
    private func startMeasurementScreeen(_ mode: ShowMeasurementMode) {
        let vc = MeasurementVC()
        
        let analyzer: SignalAnalyzerProtocol = SignalAnalizer()
        var source: SignalSourceProtocol
        var timer: TimerCreator
        
        
        switch mode {
            case .new5Minutes:
                source = RandomSourceService()
                timer = Timer.fiveMinutes
            case .newEndless:
                source = RandomSourceService()
                timer = Timer.endless
            case .withId(let id):
                if let data = Storage.getRecord(id: id) {
                    source = PredefinedSignalSource(data)
                } else {
                    source = RandomSourceService()
                }
                timer = Timer.endless
        }
        
        let vm = MeasurementViewModel(self, source: source, analyzer: analyzer, timer: timer)
        vc.vm = vm
        self.navigationController.pushViewController(vc, animated: true)
    }
}

protocol MeasurementListCoordinaror {
    func showMeasurement(_ mode: ShowMeasurementMode)
    func requestNewMeasurement()
}

protocol MeasurementScreenCoordinator {
    func stopMeasurement(_ reason: MeasurementViewModel.StopReason)
}

enum ShowMeasurementMode {
    case new5Minutes
    case newEndless
    case withId(Int)
}
