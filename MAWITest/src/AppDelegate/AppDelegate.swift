//
//  AppDelegate.swift
//  MAWITest
//
//  Created by admin on 01/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit
import BackgroundTasks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private var coordinator: RootCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let navController = UINavigationController()
        self.coordinator = RootCoordinator(navigationController: navController)
        self.coordinator?.start()
               
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navController
        self.window?.makeKeyAndVisible()
        self.registerBackgroundTaks()
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        guard let _ = RandomSourceService.forcePushSignal else { return }
        self.cancelAllPendingBGTask()
        self.scheduleDataGenerating()
    }

    //MARK: BackGround Tasks
    private func registerBackgroundTaks() {
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "com.MAWI.generateData", using: nil) { task in
//            self.scheduleDataGenerating()
            self.handleDataGeneratingTask(task: task as! BGProcessingTask)
        }
    }
    
    private func cancelAllPendingBGTask() {
        BGTaskScheduler.shared.cancelAllTaskRequests()
    }
    
    private func scheduleDataGenerating() {
        let request = BGAppRefreshTaskRequest(identifier: "com.MAWI.generateData")
//        request.requiresNetworkConnectivity = false
//        request.requiresExternalPower = false
        
        request.earliestBeginDate = Date(timeIntervalSinceNow: 0.25)
        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            print("Could not schedule data generating: \(error)")
        }
    }
    
    private func handleDataGeneratingTask(task: BGProcessingTask) {
        self.scheduleDataGenerating()
        //Todo Work
        
        RandomSourceService.forcePushSignal?()
        task.expirationHandler = {}
        task.setTaskCompleted(success: true)
    }
}

