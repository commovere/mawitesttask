//
//  MeasurementVC.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit
import Realm

class MeasurementVC: UIViewController {
    var vm: MeasurementViewModel!
    override func loadView() {
        super.loadView()
        self.view = MeasurementView(vm)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.vm.start()
    }
}
