//
//  MeasurementListViewModel.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RxSwift
class MeasurementListViewModel {
    private var coordinator: MeasurementListCoordinaror
    
    let bag = DisposeBag()
    lazy var dataSource: Observable<[String]> = Storage.recordsMeta
        .map{ metas in return metas.map{"[\($0.id)] \($0.date)"} }
    
    init(_ coordinator: MeasurementListCoordinaror) {
        self.coordinator = coordinator
    }
    
    func startNewMeasurement() {
        self.coordinator.requestNewMeasurement()
    }
    
    func showMeasurement(at indexPath: IndexPath) {
        print("show at \(indexPath.row)")
        let id = indexPath.row
        self.coordinator.showMeasurement(.withId(id))
    }
}
