//
//  RandomSourceService.swift
//  MAWITest
//
//  Created by admin on 03/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RxSwift
class RandomSourceService: SignalSourceProtocol {
    static var forcePushSignal:(()->())?
    let needToSave: Bool = true
    
    private static let packSize = 120
    private static let valueRange: ClosedRange<Int16> = -2048...2048
    private static var randomPack: [Int16] {
        return (0..<packSize).map{_ in Int16.random(in: valueRange)}
    }
    
    private let bag = DisposeBag()
    private var timer: Observable<Int>?
    let dataSource = PublishSubject<[Int16]>()
    
    func start(){
        let pushSignal: ()->() = {[weak dataSource] in dataSource?.onNext(RandomSourceService.randomPack)}
        Timer.repeating(every: .milliseconds(250))
            .bind{[pushSignal] _ in pushSignal() }
            .disposed(by: self.bag)
        
        RandomSourceService.forcePushSignal = pushSignal
    }
    
    deinit { RandomSourceService.forcePushSignal = nil }
}
