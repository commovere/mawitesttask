//
//  SignalSourceProtocol.swift
//  MAWITest
//
//  Created by admin on 07/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RxSwift
protocol SignalSourceProtocol {
    func start()
    var dataSource: PublishSubject<[Int16]> {get}
    var needToSave: Bool {get}
}
