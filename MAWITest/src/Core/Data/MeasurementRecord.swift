//
//  MeasurementRecord.swift
//  MAWITest
//
//  Created by admin on 07/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RealmSwift
class MeasurementRecordMetadata: Object {
    @objc dynamic var id: Int = -1
    @objc dynamic var date: Date = Date()
}

class MeasurementRecordData: Object {
    @objc dynamic var id: Int = -1
    let values = List<Int16>()
}

struct MeasurementRecord {
    var data: MeasurementRecordData
    var meta: MeasurementRecordMetadata
    
    init(_ values: [Int16], date: Date, id: Int) {
        self.data = MeasurementRecordData()
        self.data.values.append(objectsIn: values)
        self.data.id = id
        
        self.meta = MeasurementRecordMetadata()
        self.meta.date = date
        self.meta.id = id
    }
}
