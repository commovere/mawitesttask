//
//  MeasurementListView.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import PinLayout
import RxCocoa

class MeasurementListView: UIView {
    private static let cellId = "cellId"
    
    private weak var newMeasurementButton: UIButton!
    private weak var tableView: UITableView!
    private var viewModel: MeasurementListViewModel!
    //MARK:- init
    init(_ viewModel: MeasurementListViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        self.setupComponents()
        self.setupViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    //MARK:- LyfeCycle (overrides)
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.viewModel == nil { return }
        
        self.newMeasurementButton.pin
            .width(300)
            .bottom(pin.safeArea.bottom + 10)
            .hCenter()
        self.tableView.pin
            .horizontally(10)
            .top(pin.safeArea.top + 10)
            .above(of: self.newMeasurementButton).marginBottom(10)
    }
    //MARK:- private
    private func setupComponents() {
        if self.viewModel == nil { return }
        
        self.backgroundColor = .lightGray
        self.newMeasurementButton = UI.btn("Start new measurement").add(to: self)
        
        self.tableView = UITableView().add(to: self)
        self.tableView.backgroundColor = .darkGray
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: MeasurementListView.cellId)
        
        self.layoutIfNeeded()
    }
    
    private func setupViewModel() {
        self.newMeasurementButton.rx.tap
            .bind(onNext: self.viewModel.startNewMeasurement)
            .disposed(by: self.viewModel.bag)
        
        self.viewModel.dataSource
            .bind(to: self.tableView.rx.items){ table, _, data in
                let cell = table.dequeueReusableCell(withIdentifier: MeasurementListView.cellId)!
                cell.textLabel?.text = data
                return cell
            }
            .disposed(by: self.viewModel.bag)
        
        self.tableView.rx.itemSelected
            .bind{[weak self] indexPath in
                self?.viewModel.showMeasurement(at: indexPath)
            }
            .disposed(by: self.viewModel.bag)
    }
}
