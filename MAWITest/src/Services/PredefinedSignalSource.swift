//
//  RealmSignalSource.swift
//  MAWITest
//
//  Created by admin on 07/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RxSwift
class PredefinedSignalSource: SignalSourceProtocol {
    let needToSave: Bool = false
    
    private let packSize = 120
    private let bag = DisposeBag()
    private var currentPackStart = 0
    private let initialData: [Int16]
    let dataSource = PublishSubject<[Int16]>()
    
    init(_ initialData: [Int16]) {
        self.initialData = initialData
    }
    
    func start(){
        Timer.repeating(every: .milliseconds(250))
            .bind{[weak self] _ in
                guard let self = self else { return }
                let (pack, haveMoreData) = self.nextDataPack()
                self.dataSource.onNext(pack)
                if haveMoreData { return }
                self.dataSource.onCompleted()
            }
            .disposed(by: self.bag)
    }
    
    private func nextDataPack() -> ([Int16], Bool) {
        guard initialData.count - currentPackStart >= packSize else {return ([], false)}
        let range = currentPackStart..<(currentPackStart + packSize)
        let pack = Array(initialData[range])
        self.currentPackStart += packSize
        return (pack, initialData.count - currentPackStart >= packSize)
    }
}
