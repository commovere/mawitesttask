//
//  Timer.swift
//  MAWITest
//
//  Created by admin on 07/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RxSwift
typealias TimerCreator = () -> Observable<Int>
struct Timer {
    private static let scheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private init(){}
    
    static let fiveMinutes: TimerCreator = {
         return Observable<Int>
            .timer(.seconds(300), period: .seconds(1), scheduler: scheduler)
    }
    
    static var endless: TimerCreator = {
         return Observable<Int>
            .timer(.never, period: .seconds(1), scheduler: scheduler)
    }
    
    static func repeating(every period: RxSwift.RxTimeInterval) -> Observable<Int> {
        return Observable<Int>
        .timer(.seconds(0), period: period, scheduler: scheduler)
    }
}
            
