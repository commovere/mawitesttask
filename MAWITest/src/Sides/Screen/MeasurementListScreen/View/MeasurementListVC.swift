//
//  MeasurementListVC.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit
import Realm
class MeasurementListVC: UIViewController {
    var vm: MeasurementListViewModel!
    
    override func loadView() {
        super.loadView()
        self.view = MeasurementListView(vm)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
}
