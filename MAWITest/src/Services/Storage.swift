//
//  Storage.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RealmSwift
import RxRealm
import RxSwift

struct Storage {
    private init(){}
    
    static func save(_ newRecordData: [Int16]){
        let id = getNextId()
        let realm = try! Realm()
        let record = MeasurementRecord(newRecordData, date: Date(), id: id)
//        print(realm.configuration.fileURL)
        try! realm.write{
            realm.add(record.data)
            realm.add(record.meta)
        }
    }
    
    static let recordsMeta: Observable<[MeasurementRecordMetadata]> = {
        let realm = try! Realm()
        let meta = realm.objects(MeasurementRecordMetadata.self)
        return Observable.array(from: meta)
    }()
    
    static func getRecord(id: Int) -> [Int16]? {
        let realm = try! Realm()
        guard let r =  realm.objects(MeasurementRecordData.self).filter("id = \(id)").first else { return nil}
        return Array(r.values)
    }
    
    static private var _nextId = -1
    static private func getNextId() -> Int {
        defer { _nextId = _nextId + 1 }
        guard _nextId < 0 else { return _nextId }
        let realm = try! Realm()
        _nextId = realm.objects(MeasurementRecordMetadata.self).count
        return _nextId
    }
}
