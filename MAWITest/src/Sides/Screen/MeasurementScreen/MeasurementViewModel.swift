//
//  MeasurementViewModel.swift
//  MAWITest
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import RxSwift
import Charts

class MeasurementViewModel {
    enum StopReason: String {
        case byTimer = "time up"
        case byUser = "stoped by user"
        case byError = "5 erros in a row"
    }
    private var coordinator: MeasurementScreenCoordinator
    
    private var rawData: [Int16] = []
    private var source: SignalSourceProtocol
    private var analyzer: SignalAnalyzerProtocol
    private var timerCreator: TimerCreator
    let state = PublishSubject<String>()
    let dataSource = PublishSubject<LineChartData>()
    let bag = DisposeBag()
    
    init(_ coordinator: MeasurementScreenCoordinator, source: SignalSourceProtocol, analyzer: SignalAnalyzerProtocol, timer timerCreator: @escaping TimerCreator) {
        self.coordinator = coordinator
        self.source = source
        self.analyzer = analyzer
        self.timerCreator = timerCreator
    }
    
    func start() {
        self.source.dataSource
            .bind{[weak self] data in
                guard let self = self else { return }
                self.dataSource.onNext(self.chartData(for: data))
                self.analyzer.append(signalChunk: data)
                
                guard self.source.needToSave else { return }
                self.rawData.append(contentsOf: data)
            }
            .disposed(by: self.bag)
        self.analyzer.errorsInRow
            .bind{[weak self] value in
                self?.state.onNext("State: \(value == 0 ? "valid": "invalid")")
                if value >= 5 { self?.stop(.byError) }
            }
            .disposed(by: self.bag)
        self.source.start()
        
        self.timerCreator()
            .bind{[weak self] _ in self?.stop(.byTimer)}
            .disposed(by: self.bag)
    }
    
    func stop(_ reason: StopReason) {
        self.coordinator.stopMeasurement(reason)
        
        guard self.source.needToSave else { return }
        Storage.save(self.rawData)
    }
    
    private func chartData(for pack: [Int16]) -> LineChartData {
        let lineDataEntries = (0..<pack.count).map{ChartDataEntry(x: Double($0), y: Double(pack[$0]))}
        let lineDataSet = LineChartDataSet(entries: lineDataEntries)
        lineDataSet.drawCirclesEnabled = false
        return LineChartData(dataSet: lineDataSet)
    }
}
